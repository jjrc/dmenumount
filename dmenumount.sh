#!/bin/bash
# dmenumount - mount devices through dmenu.
# Source code.

if [ -f "$XDG_CONFIG_HOME/dmenumount/config.sh" ]; then
	. "$XDG_CONFIG_HOME/dmenumount/config.sh"
else
	. "/usr/local/etc/dmenumount/config.sh"
fi

DMENUCMD () {
	echo -e "$2" | dmenu -m $DMENUMON -fn "$DMENUFONT" -nb "$COL_NORMAL_BG" -nf "$COL_NORMAL_FG" -sb "$COL_SELECT_BG" -sf "$COL_SELECT_FG" -i -p "$1"
}

MOUNT_ASK () {
	MOUNT_ANS=$(DMENUCMD "¿Mount or umount?" "Mount\nUmount") || exit 1
}

UMOUNT_ASK () {
	UMOUNT_ANS=$(DMENUCMD "¿Unmount '$MOUNTPOINT_DEVICE'?" "No\nYes" ) || exit 1
}

SELECT_MOUNT_PARTITION () {
	INFO=$(lsblk -P -o "name,label,fstype,size,type,mountpoint,UUID" | grep 'TYPE="part".*MOUNTPOINT=""')
	LIST_NAME=$(echo "$INFO" | cut -d ' ' -f1 | cut -d '=' -f2 | sed 's/"//g')
	LIST_LABEL=$(echo "$INFO" | cut -d ' ' -f2 | cut -d '=' -f2 | sed 's/"//g')
	LIST_FSTYPE=$(echo "$INFO" | cut -d ' ' -f3 | cut -d '=' -f2 | sed 's/"//g')
	LIST_SIZE=$(echo "$INFO" | cut -d ' ' -f4 | cut -d '=' -f2 | sed 's/"//g')
	LIST_UUID=$(echo "$INFO" | cut -d ' ' -f7 | cut -d '=' -f2 | sed 's/"//g')

	SELECTION_RAW=$(paste -d ';' <(echo "$LIST_NAME" | sed 's/^/\/dev\//g') <(echo "$LIST_LABEL") <(echo "$LIST_FSTYPE") <(echo "$LIST_UUID"))
	SELECTION_FORMAT=$(paste -d ' ' <(echo "$LIST_NAME" | sed 's/^/\/dev\//g') <(echo "$LIST_LABEL" | sed 's/^/\(label: /g' | sed 's/$/,/g') <(echo "$LIST_SIZE" | sed 's/^/size: /g' | sed 's/$/\)/g'))
	SELECTION_GROUP=$(paste -d ';' <(echo "$SELECTION_RAW") <(echo "$SELECTION_FORMAT"))
	SELECTION_DMENU=$(DMENUCMD "Choose device to mount:" "$SELECTION_FORMAT") || exit 1

	DIR_DEVICE=$(echo "$SELECTION_GROUP" | grep "$SELECTION_DMENU"  | cut -d ';' -f1)
	LABEL_DEVICE=$(echo "$SELECTION_GROUP" | grep "$SELECTION_DMENU"  | cut -d ';' -f2)
	FSTYPE_DEVICE=$(echo "$SELECTION_GROUP" | grep "$SELECTION_DMENU"  | cut -d ';' -f3) 
	UUID_DEVICE=$(echo "$SELECTION_GROUP" | grep "$SELECTION_DMENU"  | cut -d ';' -f4) 
}

SELECT_UMOUNT_PARTITION () {
	INFO=$(lsblk -P -o "name,label,size,type,mountpoint,UUID" | grep 'TYPE="part"' | grep -v 'MOUNTPOINT=""')
	LIST_NAME=$(echo "$INFO" | cut -d ' ' -f1 | cut -d '=' -f2 | sed 's/"//g')
	LIST_LABEL=$(echo "$INFO" | cut -d ' ' -f2 | cut -d '=' -f2 | sed 's/"//g')
	LIST_SIZE=$(echo "$INFO" | cut -d ' ' -f3 | cut -d '=' -f2 | sed 's/"//g')
	LIST_MOUNTPOINT=$(echo "$INFO" | cut -d ' ' -f5 | cut -d '=' -f2 | sed 's/"//g')
	LIST_UUID=$(echo "$INFO" | cut -d ' ' -f6 | cut -d '=' -f2 | sed 's/"//g')

	SELECTION_RAW=$(paste -d ';' <(echo "$LIST_NAME" | sed 's/^/\/dev\//g') <(echo "$LIST_MOUNTPOINT") <(echo "$LIST_UUID"))
	SELECTION_FORMAT=$(paste -d ' ' <(echo "$LIST_MOUNTPOINT") <(echo "$LIST_LABEL" | sed 's/^/\(label: /g' | sed 's/$/,/g') <(echo "$LIST_SIZE" | sed 's/^/size: /g' | sed 's/$/\)/g'))
	SELECTION_GROUP=$(paste -d ';' <(echo "$SELECTION_RAW") <(echo "$SELECTION_FORMAT"))
	SELECTION_DMENU=$(DMENUCMD "Choose device to umount:" "$SELECTION_FORMAT") || exit 1

	DIR_DEVICE=$(echo "$SELECTION_GROUP" | grep "$SELECTION_DMENU"  | cut -d ';' -f1)
	MOUNTPOINT_DEVICE=$(echo "$SELECTION_GROUP" | grep "$SELECTION_DMENU"  | cut -d ';' -f2)
	UUID_DEVICE=$(echo "$SELECTION_GROUP" | grep "$SELECTION_DMENU"  | cut -d ';' -f3) 
}

MOUNT_PARTITION () {
	if [ -z "$(grep "$UUID_DEVICE" /etc/fstab 2> /dev/null)" ]; then
		[ "$LABEL_DEVICE" == "null" ] || DIR_MOUNT="$DIR_MOUNT_PREFIX/$LABEL_DEVICE" 
		[ "$LABEL_DEVICE" == "null" ] && DIR_MOUNT="$DIR_MOUNT_PREFIX/$UUID_DEVICE" 
		mkdir -p "$DIR_MOUNT"
		if [ "$FSTYPE_DEVICE" == "ntfs" -o "$FSTYPE_DEVICE" == "vfat" ]; then
			sudo -A mount -t "$FSTYPE_DEVICE" -o uid=1000,dmask=022,fmask=133 "$DIR_DEVICE" "$DIR_MOUNT" || exit 1
		elif [ "$FSTYPE_DEVICE" == "ext4" ]; then
			sudo -A mount -t "$FSTYPE_DEVICE" "$DIR_DEVICE" "$DIR_MOUNT" || exit 1
		fi
	else
		sudo -A mount "$DIR_DEVICE" || exit 1
	fi
}

UMOUNT_PARTITION () {
	UMOUNT_ASK
	if [ "$UMOUNT_ANS" == "Yes" ]; then
		sudo -A umount "$DIR_DEVICE" || exit 1
		[ -z "$(grep "$UUID_DEVICE" /etc/fstab 2> /dev/null)" ] && rmdir "$MOUNTPOINT_DEVICE"
	fi
}

MOUNT_ASK
if [ "$MOUNT_ANS" == "Mount" ]; then
	SELECT_MOUNT_PARTITION 
	MOUNT_PARTITION
elif [ "$MOUNT_ANS" == "Umount" ]; then
	SELECT_UMOUNT_PARTITION 
	UMOUNT_PARTITION
fi


# dmenumount - mount devices through dmenu.
# Configuration file.  

# color squeme 
COL_GRAY1="#222222"
COL_GRAY2="#bbbbbb"
COL_GRAY3="#eeeeee"
COL_GOLD="#806000"

# appearance
DMENUFONT="monospace:size=11"
COL_NORMAL_BG="$COL_GRAY1"
COL_NORMAL_FG="$COL_GRAY2"
COL_SELECT_BG="$COL_GOLD"
COL_SELECT_FG="$COL_GRAY3"
DMENUMON="0"

# prefix for mount directory
DIR_MOUNT_PREFIX="$HOME/mpoints"

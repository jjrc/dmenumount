# dmenumount - mount devices through dmenu. 
# Makefile.

NAME=dmenumount
PREFIX=/usr/local
VERSION=0.2

options:
	@echo no compilation is needed:
	@echo - run \"make install\" to install $(NAME)
	@echo - run \"make uninstall\" to uninstall $(NAME)

install: config.sh $(NAME).sh
	mkdir -p $(PREFIX)/bin
	mkdir -p $(PREFIX)/etc/$(NAME)
	cp -f config.sh $(PREFIX)/etc/$(NAME)
	cp -f $(NAME).sh $(PREFIX)/bin/$(NAME)
	chmod 644 $(PREFIX)/etc/$(NAME)/config.sh
	chmod 755 $(PREFIX)/bin/$(NAME)
	@echo
	@echo dmenumount \(version $(VERSION)\) installed.

uninstall:
	rm -f $(PREFIX)/bin/$(NAME)
	rm -rf $(PREFIX)/etc/$(NAME)
	@echo
	@echo dmenumount \(version $(VERSION)\) uninstalled.

.PHONY: options install uninstall
